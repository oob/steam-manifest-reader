#include <filesystem>
#include <fstream>
#include <iostream>
#include <ios>

#include <vector>
#include <map>
#include <functional>

#include <cstring>
#include <cstdint>
#include <array>
#include <span>
#include <type_traits>
#include <algorithm>
#include <bit>
#include <future>

#ifndef DEBUG_PRINT
#define DEBUG_PRINT (0)
#endif

namespace sha
{
// sha-1 implementation based on http://dx.doi.org/10.6028/NIST.FIPS.180-4

enum BlockSize
{
    Sha1 = 512 >> 3,
    Sha256 = 512 >> 3,
    Sha512 = 1024 >> 3,
    Sha1_32 = Sha1 >> 2,
    Sha256_32 = Sha256 >> 2,
    Sha512_64 = Sha512 >> 3
};

static constexpr std::uint32_t f_ch(const std::uint32_t x, const std::uint32_t y, const std::uint32_t z)
{
    // 0..20
    return (x & y) ^ (~x & z);
}

static constexpr std::uint32_t f_par(const std::uint32_t x, const std::uint32_t y, const std::uint32_t z)
{
    // 20..40,60..80
    return x ^ y ^ z;
}

static constexpr std::uint32_t f_maj(const std::uint32_t x, const std::uint32_t y, const std::uint32_t z)
{
    // 40..60
    return (x & y) ^ (x & z) ^ (y & z);
}

template <
    typename T,
    typename = std::enable_if<std::is_integral<T>::value, T>::type,
    std::size_t N
>
std::array<T, N>& operator +=(std::array<T, N> &l, const std::array<T, N> &r)
{
    for (std::size_t i = 0; i < N; i+=1)
    {
        l[i] += r[i];
    }
    return l;
}

static std::array<std::uint32_t, 5> updateHash(const std::span<std::uint32_t> &M, const std::array<std::uint32_t, 5> &H)
{
    constexpr std::uint32_t k_ch = 0x5a827999;
    constexpr std::uint32_t k_par1 = 0x6ed9eba1;
    constexpr std::uint32_t k_maj = 0x8f1bbcdc;
    constexpr std::uint32_t k_par2 = 0xca62c1d6;

    // continue with hash values from previous block
    std::array<std::uint32_t, 5> h = H;
    std::array<std::uint32_t, 80> W;
    std::uint32_t T;
    unsigned int t = 0;

    // first 16 iterations are split for filling inital part of message schedule W
    for (; t < 16; ++t)
    {
        W[t] = M[t];
        T = std::rotl(h[0], 5) + f_ch(h[1], h[2], h[3]) + h[4] + k_ch + W[t];
        h = { T, h[0], std::rotl(h[1], 30), h[2], h[3] };
    }
    // t=0..20 uses f_ch,k_ch
    for (; t < 20; ++t)
    {
        T = W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16];
        W[t] = std::rotl(T, 1);
        T = std::rotl(h[0], 5) + f_ch(h[1], h[2], h[3]) + h[4] + k_ch + W[t];
        h = { T, h[0], std::rotl(h[1], 30), h[2], h[3] };
    }
    // t=20..40 uses f_par,k_par1
    for (; t < 40; ++t)
    {
        T = W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16];
        W[t] = std::rotl(T, 1);
        T = std::rotl(h[0], 5) + f_par(h[1], h[2], h[3]) + h[4] + k_par1 + W[t];
        h = { T, h[0], std::rotl(h[1], 30), h[2], h[3] };
    }
    // t=40..60 uses f_maj,k_maj
    for (; t < 60; ++t)
    {
        T = W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16];
        W[t] = std::rotl(T, 1);
        T = std::rotl(h[0], 5) + f_maj(h[1], h[2], h[3]) + h[4] + k_maj + W[t];
        h = { T, h[0], std::rotl(h[1], 30), h[2], h[3] };
    }
    // t=60..80 uses f_par,k_par2
    for (; t < 80; ++t)
    {
        T = W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16];
        W[t] = std::rotl(T, 1);
        T = std::rotl(h[0], 5) + f_par(h[1], h[2], h[3]) + h[4] + k_par2 + W[t];
        h = { T, h[0], std::rotl(h[1], 30), h[2], h[3] };
    }
    return h;
}

static std::array<std::uint32_t, 5> processBlock(std::span<std::uint32_t> &M, const std::array<std::uint32_t, 5> &H, const std::span<std::uint8_t> &buffer, const unsigned int bufferOffset)
{
    if constexpr (std::endian::native == std::endian::little)
    {
        // swap order for le
        for (unsigned int i = 0; i < BlockSize::Sha1_32; ++i)
        {
            const unsigned int s = (i * sizeof(std::uint32_t)) + bufferOffset;
            M[i] = (buffer[s] << 24) | (buffer[s+1] << 16) | (buffer[s+2] << 8) | buffer[s+3];
        }
    }
    else
    {
        // update span offsets for be
        M = std::span<std::uint32_t>(
            reinterpret_cast<std::uint32_t*>(buffer.data()+bufferOffset),
            reinterpret_cast<std::uint32_t*>(buffer.data()+bufferOffset)+BlockSize::Sha1_32
        );
    }
    return updateHash(M, H);
}

static std::array<std::uint32_t, 5> processStream(std::ifstream &&in, const std::uintmax_t streamSize)
{
    constexpr unsigned int chunkSize = 1024*1024;
    // read +1 for smaller sizes to trigger eof on first call
    const std::size_t bufferSize = (chunkSize > streamSize) ?
        ((streamSize > BlockSize::Sha1) ? streamSize+1 : BlockSize::Sha1) :
        chunkSize;
    // buffer for incoming data, capped to blocksize or chunksize if data doesn't fit
    std::vector<std::uint8_t> buffer(bufferSize);
    std::span<std::uint8_t> Br(buffer.begin(), buffer.end());
    std::vector<std::uint8_t> buffer2(bufferSize);
    std::span<std::uint8_t> Bp(buffer2.begin(), buffer2.end());
    std::span<std::uint8_t> Bt;

    // algorithm uses 32 bit words
    std::span<std::uint32_t> M;
    // buffer for endian-swapped data
    std::array<std::uint8_t, BlockSize::Sha1> M_le;
    if constexpr (std::endian::native == std::endian::little)
    {
        M = std::span<std::uint32_t>(
            reinterpret_cast<std::uint32_t*>(M_le.data()),
            reinterpret_cast<std::uint32_t*>(M_le.data())+BlockSize::Sha1_32
        );
    }
    std::uint64_t streamOffset = 0;
    unsigned int bufferOffset = 0;

    // initial sha1 hash value
    std::array<std::uint32_t, 5> H = { 0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0 };
    if (!in.eof())
    {
        // first read is synchronous
        in.read(reinterpret_cast<char*>(Br.data()), bufferSize);
        std::size_t bytesRead = in.gcount();
        streamOffset += bytesRead;

        while (!in.eof() && bytesRead >= BlockSize::Sha1)
        {
            bufferOffset = 0;
            std::future<std::size_t> pendingRead = std::async(std::launch::async | std::launch::deferred, [&]
            {
                in.read(reinterpret_cast<char*>(Bp.data()), bufferSize);
                return static_cast<std::size_t>(in.gcount());
            });

            do {
                H += processBlock(M, H, Br, bufferOffset);
                bufferOffset += BlockSize::Sha1;
            } while (bufferOffset+BlockSize::Sha1 <= bytesRead);

            bytesRead = pendingRead.get();
            // read count can be 0 if it happens right at eof
            if (bytesRead > 0)
            {
                streamOffset += bytesRead;
                // swap spans
                Bt = Br;
                Br = Bp;
                Bp = Bt;
                bufferOffset = 0;
            }
        }
        if (bytesRead >= BlockSize::Sha1)
        {
            // process last asynchronous read
            do {
                H += processBlock(M, H, Br, bufferOffset);
                bufferOffset += BlockSize::Sha1;
            } while (bufferOffset+BlockSize::Sha1 <= bytesRead);
        }
    }
    if (streamOffset != streamSize)
    {
        std::cerr << "warn: [sha::processStream] processed size mismatch (" << streamOffset << " != " << streamSize << ")\n" << std::endl;
    }
    // reached last block, get necessary values for padding/extending the buffer
    constexpr unsigned int sizeExtra = 1+sizeof(std::uint64_t);
    const std::size_t remaining = (streamOffset & (BlockSize::Sha1-1)) + sizeExtra;
    const std::size_t k = (BlockSize::Sha1 - (remaining & (BlockSize::Sha1-1))) & (BlockSize::Sha1-1);
    const std::size_t bufferEnd = bufferOffset+k+remaining;
    if (bufferEnd > bufferSize)
    {
        if (Br.data() == buffer.data())
        {
            buffer.resize(bufferEnd);
            Br = std::span<std::uint8_t>(buffer.begin(), buffer.end());
        }
        else
        {
            buffer2.resize(bufferEnd);
            Br = std::span<std::uint8_t>(buffer2.begin(), buffer2.end());
        }
    }

    // pad by appending a 1 bit, filling the middle with 0 bytes (k)
    Br[bufferEnd-sizeExtra-k] = static_cast<std::uint8_t>(0x80);

    if (k > 0)
    {
        std::fill_n(Br.begin()+bufferEnd-8-k, k, 0x00);
    }

    // last 64 bits are bit size of processed data (l), in be
    const std::uint64_t l = streamOffset << 3;
    for (int i = 7; i >= 0; --i)
    {
        Br[bufferEnd-(i+1)] = static_cast<std::uint8_t>(l >> (i<<3));
    }

    // new data added a block for processing
    if (remaining > BlockSize::Sha1)
    {
        H += processBlock(M, H, Br, bufferOffset);
        bufferOffset += BlockSize::Sha1;
    }
    H += processBlock(M, H, Br, bufferOffset);
    return H;
}


static std::array<std::uint32_t, 5> endianSwap(std::array<std::uint32_t, 5> &&data)
{
    std::array<std::uint32_t, 5> swapped;
    for (std::size_t i = 0; i < data.size(); ++i)
    {
        swapped[i] =
            ((data[i] >> 24) & 0xff) | (((data[i] >> 16) & 0xff) << 8) |
            (((data[i] >> 8) & 0xff) << 16) | ((data[i] & 0xff) << 24);
    }
    return swapped;
}
}

template <typename T>
class DataReader
{
    private:
        std::vector<T> data;
        std::size_t offset;

    public:
        DataReader(std::vector<T> &&in) : data(std::move(in)), offset(0)
        {}

        DataReader(const std::size_t size) : data(), offset(0)
        {
            data.reserve(size);
        }

        // caller should check in advance whether data exists for reading (eg with hasData())
        T read()
        {
            if (offset < data.size())
            {
                const T v = data[offset];
                offset += 1;
                return v;
            }
            return T();
        }

        std::size_t read(T *dest, const std::size_t count)
        {
            if (dest && offset < data.size() && count > 0)
            {
                const std::size_t r = (offset+count >= data.size()) ? (data.size()-offset) : count;
                std::memcpy(dest, data.data()+offset, r*sizeof(T));
                offset += r;
                return r;
            }
            return 0;
        }

        std::size_t read(std::vector<T> *dest, const std::size_t count)
        {
            if (dest && offset < data.size() && count > 0)
            {
                const std::size_t r = (offset+count >= data.size()) ? (data.size()-offset) : count;
                std::copy(data.begin()+offset, data.begin()+offset+r, dest->begin());
                offset += r;
                return r;
            }
            return 0;
        }

        std::size_t write(const std::vector<T> &in, const std::size_t count)
        {
            std::copy(in.begin(), in.begin()+count, data.begin()+offset);
            return data.size() - offset;
        }

        const std::vector<T>& getData() const
        {
            return data;
        }

        int hasData() const
        {
            return static_cast<int>(data.size() - offset);
        }
};

namespace proto
{
// protobuf encoding description (varint, message structure) -> https://protobuf.dev/programming-guides/encoding/
template <typename T>
struct VarInt
{
private:
    std::vector<std::uint8_t> bytes;

public:
    std::uint64_t decode(T &input)
    {
        bytes.clear();
        std::uint8_t tmp = 0xff;
        std::size_t res;
        // read until continuation bit is 0
        do {
            if constexpr (std::is_same<T, std::ifstream>::value)
            {
                input.read(reinterpret_cast<char*>(&tmp), 1);
                res = 1;
            }
            else
            {
                res = input.read(&tmp, 1);
            }
            bytes.push_back(tmp & 0x7f);
        } while (res > 0 && tmp & 0x80);

        if (res == 0 && (tmp & 0x80))
        {
            throw std::out_of_range("reached early end of data for varint decode");
        }

        // store as largest possible type and convert to little endian (continuation bit is discarded)
        std::uint64_t value;
        switch (bytes.size())
        {
            case 1:
                value = bytes[0];
                break;
            case 2:
                value = (bytes[1] << 7) | bytes[0];
                break;
            case 3:
                value = (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 4:
                value = (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 5:
                value = (static_cast<std::uint64_t>(bytes[4]) << 28) |
                        (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 6:
                value = (static_cast<std::uint64_t>(bytes[5]) << 35) | (static_cast<std::uint64_t>(bytes[4]) << 28) |
                        (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 7:
                value = (static_cast<std::uint64_t>(bytes[6]) << 42) |
                        (static_cast<std::uint64_t>(bytes[5]) << 35) | (static_cast<std::uint64_t>(bytes[4]) << 28) |
                        (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 8:
                value = (static_cast<std::uint64_t>(bytes[7]) << 49) | (static_cast<std::uint64_t>(bytes[6]) << 42) |
                        (static_cast<std::uint64_t>(bytes[5]) << 35) | (static_cast<std::uint64_t>(bytes[4]) << 28) |
                        (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 9:
                value = (static_cast<std::uint64_t>(bytes[8]) << 56) |
                        (static_cast<std::uint64_t>(bytes[7]) << 49) | (static_cast<std::uint64_t>(bytes[6]) << 42) |
                        (static_cast<std::uint64_t>(bytes[5]) << 35) | (static_cast<std::uint64_t>(bytes[4]) << 28) |
                        (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            case 10:
                // upper 6 bits of 10th byte exceed uint64 range
                if (bytes[9] & 0xfe)
                {
                    throw std::out_of_range("decoded varint value exceeding 64 bit range");
                }
                value = (static_cast<std::uint64_t>(bytes[9]) << 63) | (static_cast<std::uint64_t>(bytes[8]) << 56) |
                        (static_cast<std::uint64_t>(bytes[7]) << 49) | (static_cast<std::uint64_t>(bytes[6]) << 42) |
                        (static_cast<std::uint64_t>(bytes[5]) << 35) | (static_cast<std::uint64_t>(bytes[4]) << 28) |
                        (bytes[3] << 21) | (bytes[2] << 14) | (bytes[1] << 7) | bytes[0];
                break;
            default:
                throw std::out_of_range("invalid varint byte size (" + std::to_string(bytes.size()) + " bytes)");
        }
        return value;
    }

#if DEBUG_PRINT
    unsigned int getSize() const
    {
        return bytes.size();
    }
#endif
};

template <typename T>
struct Record
{
    enum class WireType : std::uint8_t
    {
        VarInt = 0,
        I64,
        Len,
        Sgroup,
        Egroup,
        I32
    };

    WireType type;
    unsigned int fieldNumber;
    std::vector<std::uint8_t> buffer;
    std::uint64_t value;
#if DEBUG_PRINT
    unsigned int valueSize;    // size of the associated data
    unsigned int recordSize;   // size of the record (without its data)
#endif

    Record(T &input)
    {
        read(input);
    }

    void getBytes(std::ifstream &stream, std::uint64_t length)
    {
        stream.read(reinterpret_cast<char*>(buffer.data()), length);
    }

    void getBytes(DataReader<std::uint8_t> &data, std::uint64_t length)
    {
        data.read(buffer.data(), length);
    }

#if DEBUG_PRINT
    int getSize() const
    {
        switch (type)
        {
            case WireType::VarInt: return valueSize + recordSize;
            case WireType::Len: return buffer.size() + recordSize;
            case WireType::I64: return sizeof(std::uint64_t) + recordSize;
            case WireType::I32: return sizeof(std::uint32_t) + recordSize;
            default: return -1;
        }
    }

    const char* getType() const
    {
        switch (type)
        {
            case WireType::VarInt: return "VarInt";
            case WireType::Len: return "Len";
            case WireType::I64: return "I64";
            case WireType::I32: return "I32";
            case WireType::Sgroup: return "Sgroup";
            case WireType::Egroup: return "Egroup";
            default: return "Unknown";
        }
    }
#endif

private:
    void read(T &input)
    {
        VarInt<T> vint;
        try {
            const auto record = vint.decode(input);
            fieldNumber = record >> 3;
            type = static_cast<WireType>(record & 0x7);
#if DEBUG_PRINT
            recordSize = vint.getSize();
#endif
        }
        catch (const std::exception &e)
        {
            throw std::runtime_error(std::string("failed to read record: ") + e.what());
        }
#if DEBUG_PRINT
        if (recordSize != 1)
        {
            throw std::out_of_range("unexpected size for record info varint: {fieldNumber=" + std::to_string(fieldNumber) + ", type=" + getType() + ", size=" + std::to_string(recordSize) + "}");
        }
#endif
        switch (type)
        {
            case WireType::VarInt:
            {
                try {
                    value = vint.decode(input);
                }
                catch (const std::exception &e)
                {
                    throw std::runtime_error("failed to read record{field=" + std::to_string(fieldNumber) + ", type=VarInt} value: " + e.what());
                }
#if DEBUG_PRINT
                valueSize = vint.getSize();
#endif
                break;
            }
            case WireType::Len:
            {
                std::uint64_t length;
                try {
                    length = vint.decode(input);
                }
                catch (const std::exception &e)
                {
                    throw std::runtime_error("failed to read record{field=" + std::to_string(fieldNumber) + ", type=Len} length: " + e.what());
                }
#if DEBUG_PRINT
                recordSize += vint.getSize();
#endif
                buffer.resize(length);
                getBytes(input, length);
                break;
            }
            case WireType::I64:
            {
                buffer.resize(sizeof(std::uint64_t));
                getBytes(input, sizeof(std::uint64_t));
                value = (static_cast<std::uint64_t>(buffer[7]) << 56) | (static_cast<std::uint64_t>(buffer[6]) << 48) |
                        (static_cast<std::uint64_t>(buffer[5]) << 40) | (static_cast<std::uint64_t>(buffer[4]) << 32) |
                        (buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | buffer[0];
                break;
            }
            case WireType::I32:
            {
                buffer.resize(sizeof(std::uint32_t));
                getBytes(input, sizeof(std::uint32_t));
                value = (buffer[3] << 24) | (buffer[2] << 16) | (buffer[1] << 8) | buffer[0];
                break;
            }
            default:
                throw std::invalid_argument("unimplemented wire type " + std::to_string(static_cast<std::uint8_t>(type)));
        }
    }
};

struct RecordInfo
{
    const char *name;
    bool optional;
    bool repeated;
    const std::function<void(const Record<DataReader<std::uint8_t>> &)> assigner;
};
}

enum class OutputFlags : unsigned int
{
    None = 0,
    Name = 1 << 0,
    Hash = 1 << 1,
    NameHash = 1 << 2,
    LinkName = 1 << 3,
    Size = 1 << 4,
    Flags = 1 << 5,
    ChunksShort = 1 << 6,
    ChunksFull = 1 << 7,
    Default = Name | Hash | Size | ChunksShort,
    Verbose = Default | LinkName | Flags,
    All = Verbose | NameHash | ChunksFull
};
OutputFlags operator |(const OutputFlags &lhs, const OutputFlags &rhs)
{
    return static_cast<OutputFlags>(static_cast<unsigned int>(lhs) | static_cast<unsigned int>(rhs));
}
OutputFlags& operator |=(OutputFlags &self, const OutputFlags &flags)
{
    self = self | flags;
    return self;
}
static inline bool hasOutputFlags(const OutputFlags lhs, const OutputFlags rhs)
{
    return (static_cast<unsigned int>(lhs) & static_cast<unsigned int>(rhs)) == static_cast<unsigned int>(rhs);
}

namespace manifest
{
class FileEntry
{
private:
    DataReader<std::uint8_t> data;
    // file flags, taken from https://github.com/SteamRE/SteamKit/blob/master/Resources/SteamLanguage/enums.steamd
    enum EntryFlags
    {
        None = 0,
        UserConfig = 1 << 0,
        VersionedUserConfig = 1 << 1,
        Encrypted = 1 << 2,
        ReadOnly = 1 << 3,
        Hidden = 1 << 4,
        Executable = 1 << 5,
        Directory = 1 << 6,
        CustomExecutable = 1 << 7,
        InstallScript = 1 << 8,
        Symlink = 1 << 9
    };

    std::string name;
    std::uint64_t size;
    unsigned int flags;
    std::vector<std::uint8_t> sha1Name;
    std::vector<std::uint8_t> sha1Data;
    std::vector<std::uint8_t> chunkData;
    std::string linkName;

    std::unordered_map<unsigned int, unsigned int> mappedData;

    // map according to manifest protobuf definition file -> https://github.com/SteamDatabase/Protobufs/blob/master/steam/content_manifest.proto
    // todo: parse chunk data as well
    const std::map<unsigned int, proto::RecordInfo> mapping = {
        {1, { "name", true, false, [&] (const auto &record) {name.assign(record.buffer.begin(), record.buffer.end());} }},
        {2, { "size", true, false , [&] (const auto &record) {size = record.value;} }},
        {3, { "flags", true, false, [&] (const auto &record) {flags = record.value;} }},
        {4, { "sha1Name", true, false, [&] (const auto &record) {sha1Name = record.buffer;} }},
        {5, { "sha1Data", true, false, [&] (const auto &record) {sha1Data = record.buffer;} }},
        {6, { "chunks", true, true, [&] (const auto &record) {chunkData.insert(chunkData.end(), record.buffer.begin(), record.buffer.end());} }},
        {7, { "link", true, false, [&] (const auto &record) {linkName.assign(record.buffer.begin(), record.buffer.end());} }}
    };

    // check if all required fields have been parsed at least once
    bool checkMapped() const
    {
        for (const auto &it : mapping)
        {
            if (!it.second.optional && mappedData.find(it.first) == mappedData.end())
            {
                return false;
            }
        }
        return true;
    }

    void parse(DataReader<std::uint8_t> &input)
    {
        do {
            const proto::Record<DataReader<std::uint8_t>> record(input);
            const proto::RecordInfo &recordInfo = mapping.at(record.fieldNumber);

            if (mappedData.find(record.fieldNumber) != mappedData.end())
            {
                if (recordInfo.repeated)
                {
                    mappedData[record.fieldNumber] += 1;
                }
                else
                {
                    throw std::invalid_argument("fileEntry parse: duplicated non-repeatable field number " + std::to_string(record.fieldNumber));
                }
            }
            else
            {
                mappedData.insert({record.fieldNumber, 1});
            }
#if DEBUG_PRINT
            // todo: print stream offset for easier troubleshooting (can only get relative offset from DataReader)
            std::cout << "mapping " << record.fieldNumber << ": " << recordInfo.name << "={size=0x" << record.getSize() << ", type=" << record.getType() << "}";
            if (record.fieldNumber == 6)
            {
                std::cout << "[\n";
                for (const std::uint8_t &it : record.buffer) std::cout << std::setw(2) << static_cast<unsigned int>(it) << " ";
                std::cout << "\n]" << std::endl;
            }
            else
            {
                std::cout << std::endl;
            }
#endif
            recordInfo.assigner(record);

        // read until all input data is consumed
        } while (checkMapped() && input.hasData() > 0);
    }

public:
    FileEntry(std::vector<std::uint8_t> &&input) : data(std::move(input))
    {
        parse(data);
    }

    void print(const OutputFlags printflags) const
    {
        std::ios oldState(nullptr);
        oldState.copyfmt(std::cout);
        std::cout << "{\n";
        if (hasOutputFlags(printflags, OutputFlags::Name))
        {
            std::cout << "\tname: \"" << name << "\",\n";
        }
        if (hasOutputFlags(printflags, OutputFlags::Size))
        {
            std::cout << "\tsize: " << std::dec << size << std::hex << ",\n";
        }
        if (hasOutputFlags(printflags, OutputFlags::Hash))
        {
            std::cout << "\tsha1: " << std::setfill('0');
            for (const std::uint8_t &it : sha1Data) std::cout << std::setw(2) << static_cast<unsigned int>(it);
            std::cout << ",\n";
        }
        if (hasOutputFlags(printflags, OutputFlags::NameHash))
        {
            std::cout << "\tsha1Name: " << std::setfill('0');
            for (const std::uint8_t &it : sha1Name) std::cout << std::setw(2) << static_cast<unsigned int>(it);
            std::cout << ",\n";
        }
        if (hasOutputFlags(printflags, OutputFlags::LinkName))
        {
            std::cout << "\tlinkName: \"" << linkName << "\",\n";
        }
        if (hasOutputFlags(printflags, OutputFlags::Flags))
        {
            std::cout << "\tflags: 0x" << std::setfill('0') << std::setw(3) << flags << ",\n";
        }
        if (hasOutputFlags(printflags, OutputFlags::ChunksFull))
        {
            std::cout << "\tchunkData[" << std::dec << chunkData.size() << std::hex << "]: [";
            for (const std::uint8_t &it : chunkData) std::cout << std::setw(2) << static_cast<unsigned int>(it) << " ";
            std::cout << "],\n";
        }
        else if (hasOutputFlags(printflags, OutputFlags::ChunksShort))
        {
            std::cout << "\tchunkData[" << std::dec << chunkData.size() << std::hex << "],\n";
        }
        std::cout << "}," << std::endl;
        if (flags & 0xfffffc00)
        {
            // todo: move this to parse
            std::cout << "warn: ^entry has UNKNOWN FLAGS 0x" << (flags & 0xfffffc00) << std::endl;
        }
        std::cout.copyfmt(oldState);
    }

    bool verify(const std::span<std::uint8_t> &hashData) const
    {
        if (!std::equal(hashData.begin(), hashData.end(), sha1Data.begin()))
        {
            std::ios oldState(nullptr);
            oldState.copyfmt(std::cerr);
            std::cerr << "error: [" << name << "] hash mismatch - actual=" << std::hex << std::setfill('0');
            for (const std::uint8_t &it : hashData) std::cerr << std::setw(2) << static_cast<unsigned int>(it);
            std::cerr << ", manifest=";
            for (const std::uint8_t &it : sha1Data) std::cerr << std::setw(2) << static_cast<unsigned int>(it);
            std::cerr << std::endl;
            std::cerr.copyfmt(oldState);
            return false;
        }
        return true;
    }

    bool isDirectory() const
    {
        return flags & EntryFlags::Directory;
    }

    bool isSymlink() const
    {
        return flags & EntryFlags::Symlink;
    }

    const std::string& getName() const
    {
        return name;
    }

    std::uint64_t getSize() const
    {
        return size;
    }
};

class Reader
{
private:
    enum class Magic : std::uint32_t
    {
        Header = 0x71f617d0,
        Metadata = 0x1f4812be,
        Signature = 0x1b81b817,
        Footer = 0x32c415ab
    };

    enum class ReadError
    {
        NoError,
        FileError,
        ParseError,
        VerifyError
    };

    mutable ReadError status;
    std::vector<FileEntry> files;

    void parse(std::ifstream &stream, const std::uintmax_t streamSize)
    {
        std::ios oldState(nullptr);
        oldState.copyfmt(std::cout);
        try {
            // automatically catch invalid read ranges
            stream.exceptions(std::ifstream::eofbit);
            Magic magic;
            stream.read(reinterpret_cast<char*>(&magic), sizeof(magic));
#if DEBUG_PRINT
            std::cout << "magic: 0x" << std::hex << std::setw(8) << std::setfill('0') << static_cast<std::uint32_t>(magic) << std::endl;
#else
            std::cout << std::hex << std::setfill('0');
#endif
            if (magic != Magic::Header)
            {
                throw std::invalid_argument("invalid file header: " + std::to_string(static_cast<std::uint32_t>(magic)));
            }
            std::uint32_t payloadSize;
            stream.read(reinterpret_cast<char*>(&payloadSize), sizeof(payloadSize));
            if (payloadSize > 0)
            {
                std::fpos offset = stream.tellg();
                std::fpos payloadEnd = offset + static_cast<std::streamoff>(payloadSize);
#if DEBUG_PRINT
                std::cout << "stream: payload={offset=0x" << offset << ", size=0x" << payloadSize << ", end=0x" << payloadEnd << "}" << std::endl;
#endif
                do {
                    proto::Record<std::ifstream> record(stream);
                    if (record.fieldNumber != 1)
                    {
                        throw std::invalid_argument("invalid field number for file entry record: " + std::to_string(record.fieldNumber));
                    }
#if DEBUG_PRINT
                    std::cout << "stream: parsing file[" << std::dec << files.size() << std::hex << "]={offset=0x" << (offset+static_cast<std::streamoff>(record.recordSize)) << ", size=0x" << (record.getSize() - record.recordSize) << "}" << std::endl;
#endif
                    offset = stream.tellg();
                    files.push_back(FileEntry(std::move(record.buffer)));
                } while (offset < payloadEnd);
                if (offset > payloadEnd)
                {
                    std::cout << "warn: read more than payloadSize (0x" << (offset-payloadEnd) << ")" << std::endl;
                }
            }
            else
            {
                std::cout << "warn: no manifest file data" << std::endl;
            }

            // metadata
            stream.read(reinterpret_cast<char*>(&magic), sizeof(magic));
#if DEBUG_PRINT
            std::cout << "magic: 0x" << std::setw(8) << static_cast<std::uint32_t>(magic) << std::endl;
#endif
            if (magic != Magic::Metadata)
            {
                throw std::invalid_argument("unexpected metadata magic value: " + std::to_string(static_cast<std::uint32_t>(magic)));
            }
            stream.read(reinterpret_cast<char*>(&payloadSize), sizeof(payloadSize));
            if (payloadSize > 0)
            {
                std::cout << "info: skipping metadata payload parsing (0x" << payloadSize << ")" << std::endl;
                stream.seekg(payloadSize, std::ios_base::cur);
            }
            else
            {
                std::cout << "info: empty metadata payload" << std::endl;
            }

            // signature
            stream.read(reinterpret_cast<char*>(&magic), sizeof(magic));
#if DEBUG_PRINT
            std::cout << "magic: 0x" << std::setw(8) << static_cast<std::uint32_t>(magic) << std::endl;
#endif
            if (magic != Magic::Signature)
            {
                throw std::invalid_argument("unexpected signature magic value: " + std::to_string(static_cast<std::uint32_t>(magic)));
            }
            stream.read(reinterpret_cast<char*>(&payloadSize), sizeof(payloadSize));
            if (payloadSize > 0)
            {
                std::cout << "info: skipping signature payload parsing (0x" << payloadSize << ")" << std::endl;
                stream.seekg(payloadSize, std::ios_base::cur);
            }
            else
            {
                std::cout << "info: empty signature payload" << std::endl;
            }

            // footer
            stream.read(reinterpret_cast<char*>(&magic), sizeof(magic));
#if DEBUG_PRINT
            std::cout << "magic: 0x" << std::setw(8) << static_cast<std::uint32_t>(magic) << std::endl;
#endif
            if (magic != Magic::Footer)
            {
                throw std::invalid_argument("unexpected footer magic value: " + std::to_string(static_cast<std::uint32_t>(magic)));
            }

            const std::fpos streamPos = stream.tellg();
            if (streamPos < static_cast<std::streamoff>(streamSize))
            {
                std::cout << "warn: unparsed data at end of manifest (0x" << (streamSize - streamPos) << " bytes)" << std::endl;
            }
        }
        catch (const std::exception &e)
        {
            std::cerr << "error: failed to parse file, stream read offset=0x" << stream.tellg() << "\n\t-> " << e.what() << std::endl;
            status = ReadError::ParseError;
        }
        std::cout.copyfmt(oldState);
    }

    std::size_t process(const auto &filters, const std::function<bool(const FileEntry &)> fn, const bool returnOnFail) const
    {
        std::size_t processed = 0;
        if (!filters.empty())
        {
            for (const auto &file : files)
            {
                for (const auto &filter : filters)
                {
                    if (filter.second(file.getName(), filter.first))
                    {
                        processed += 1;
                        if (!fn(file) && returnOnFail)
                        {
                            // dump manifest info about failed file
                            file.print(OutputFlags::Verbose);
                            return processed;
                        }
                        break;
                    }
                }
            }
        }
        else
        {
            for (const auto &file : files)
            {
                processed += 1;
                if (!fn(file) && returnOnFail)
                {
                    // dump manifest info about failed file
                    file.print(OutputFlags::Verbose);
                    return processed;
                }
            }
        }
        return processed;
    }

public:
    Reader(std::string &&path) : status(ReadError::NoError)
    {
        const std::filesystem::path infile(path);
        if (!std::filesystem::is_regular_file(infile))
        {
            std::cerr << "error: input file \"" << path << "\" does not exist" << std::endl;
            status = ReadError::FileError;
            return;
        }

        const std::uintmax_t size = std::filesystem::file_size(infile);
        std::ifstream instream(infile, std::ios::in | std::ios::binary);
        if (!instream.is_open())
        {
            std::cerr << "error: input file cannot be opened" << std::endl;
            status = ReadError::FileError;
            return;
        }
        parse(instream, size);
        instream.close();
    }

    void print(const auto &filters, const OutputFlags printflags) const
    {
        const auto printfn = [&] (const FileEntry &file) -> bool
        {
            file.print(printflags);
            return true;
        };
        const std::size_t count = process(filters, printfn, false);
        if (!filters.empty())
        {
            std::cerr << "info: processed " << count << " / " << files.size() << " files (" << (files.size() - count) << " skipped)" << std::endl;
        }
        else
        {
            std::cerr << "info: processed " << count << " / " << files.size() << " files" << std::endl;
        }
    }

    bool verify(const auto &filters, std::string &&dirpath, const bool strictCheck) const
    {
        const std::filesystem::path indir(dirpath);
        if (!std::filesystem::is_directory(indir))
        {
            std::cerr << "error: directory \"" << dirpath << "\" does not exist" << std::endl;
            return false;
        }
        const auto verifyfn = [&] (const FileEntry &file) -> bool
        {
            std::filesystem::path infile = indir / file.getName();
            std::cout << "info: checking \"" << infile.string() << "\" ... ";

            if (file.isDirectory())
            {
                if (std::filesystem::is_directory(infile))
                {
                    std::cout << "OK" << std::endl;
                }
                else
                {
                    std::cout << "warn: directory does not exist" << std::endl;
                }
                // ignore missing directories for now, verification will fail later anyway if there should be files in it
                return true;
            }
            else if (file.isSymlink())
            {
                if (std::filesystem::is_symlink(infile))
                {
                    // todo: also check file.linkName?
                    std::cout << "OK" << std::endl;
                }
                else
                {
                    std::cout << "warn: symlink does not exist" << std::endl;
                }
                // ignore failed symlink check for now
                return true;
            }

            // compare file size before checking the hash
            std::ifstream instream;
            std::uintmax_t size;
            try {
                size = std::filesystem::file_size(infile);
                if (size >= 10*1024*1024)
                {
                    // flush for files that could take longer to process
                    std::cout << std::flush;
                }
                if (size != file.getSize())
                {
                    std::cerr << "error: mismatched file sizes - actual=" << size << ", manifest=" << file.getSize() << std::endl;
                    return false;
                }

                instream.open(infile, std::ios::binary);
            }
            catch (const std::exception &e)
            {
                std::cerr << "error: " << e.what() << std::endl;
                return false;
            }

            auto hash = sha::processStream(std::move(instream), size);
            if constexpr (std::endian::native == std::endian::little)
            {
                hash = sha::endianSwap(std::move(hash));
            }
            std::span<std::uint8_t> hashData(
                reinterpret_cast<std::uint8_t*>(hash.data()),
                reinterpret_cast<std::uint8_t*>(hash.data())+20
            );
            if (!file.verify(hashData))
            {
                status = ReadError::VerifyError;
                return false;
            }
            std::cout << "OK" << std::endl;
            return true;
        };

        const std::size_t count = process(filters, verifyfn, strictCheck);
        if (!filters.empty())
        {
            std::cerr << "info: verified " << count << " / " << files.size() << " files (" << (files.size() - count) << " skipped)" << std::endl;
        }
        else
        {
            std::cerr << "info: verified " << count << " / " << files.size() << " files" << std::endl;
        }
        return isError();
    }

    bool isError() const
    {
        return status != ReadError::NoError;
    }
};
}

int main(int argc, char *argv[])
{
    std::string filename;
    std::string datadir;
    std::vector<
        std::pair<
            const std::string,
            const std::function<bool(const std::string &, const std::string &)>
        >
    > filters;

    // defaults, overwritten by user arguments
    OutputFlags printflags = OutputFlags::Default;
    bool explicitPrint = false;
    bool verifyAll = false;

    for (int i = 1; i < argc; ++i)
    {
        if (std::strncmp(argv[i], "-h", 3) == 0 || std::strncmp(argv[i], "--help", 7) == 0)
        {
            std::cout << "usage: " << argv[0] << " [args] <manifest-file>\n"
                "\t-h,--help              print this output and exit\n"
                "\t-d,--data <directory>  verify checksums from manifest with files on disk (steamapps)\n"
                "\t-c,--continue          only notify on mismatched checksums and check all files from manifest regardless\n"
                "\t-f,--filter <filters>  set name filters for processing files in manifest, separated by ';'\n"
                "\t                       (filter expressions can also start/end with match-all specifier '*')\n"
                "\t-p,--print [flags]     print parsed manifest data, use flags for specific output: [nhHslfcC]\n"
                "\t                       (no flags or d flag implies [nhsc], v flag adds [lf], a flag sets all flags)\n"
                << std::endl;
            return 0;
        }
        else if (std::strncmp(argv[i], "-d", 3) == 0 || std::strncmp(argv[i], "--data", 7) == 0)
        {
            i += 1;
            if (i < argc && argv[i][0] != '-')
            {
                if (!datadir.empty())
                {
                    std::cerr << "warn: \"" << argv[i] << "\" overwrites previous datadir argument \"" << datadir << "\"" << std::endl;
                }
                datadir.assign(argv[i]);
            }
            else
            {
                std::cerr << "missing argument for data path" << std::endl;
                return 1;
            }
        }
        else if (std::strncmp(argv[i], "-c", 3) == 0 || std::strncmp(argv[i], "--continue", 11) == 0)
        {
            verifyAll = true;
        }
        else if (std::strncmp(argv[i], "-f", 3) == 0 || std::strncmp(argv[i], "--filter", 9) == 0)
        {
            i += 1;
            if (i < argc && argv[i][0] != '-')
            {
                // filter options, ';' for separators, '*' for wildcards
                std::string fargs(argv[i]);
                std::string::size_type it;
                do {
                    it = fargs.find(';');
                    std::string farg = fargs.substr(0, it);
                    if (farg.length() == 1 && farg[0] == '*')
                    {
                        // match all
                        filters.push_back({std::string(), [] (const auto &file, const auto &filter) {return true;} });
                    }
                    else if (farg.length() > 2 && farg[0] == '*' && farg[farg.length()-1] == '*')
                    {
                        // match substring
                        filters.push_back({ farg.substr(1, farg.length()-2), [] (const auto &file, const auto &filter) {return file.contains(filter);} });
                    }
                    else if (farg[farg.length()-1] == '*')
                    {
                        // match beginning
                        filters.push_back({ farg.substr(0, farg.length()-1), [] (const auto &file, const auto &filter) {return file.starts_with(filter);} });
                    }
                    else if (farg[0] == '*')
                    {
                        // match end
                        filters.push_back({ farg.substr(1), [] (const auto &file, const auto &filter) {return file.ends_with(filter);} });
                    }
                    else if (farg.find('*') == std::string::npos)
                    {
                        // match exact
                        filters.push_back({ farg, [] (const auto &file, const auto &filter) {return file.compare(filter) == 0;} });
                    }
                    else
                    {
                        // todo: catch more cases ("**", "..*..", etc)
                        std::cerr << "error: unsupported filter expression \"" << farg << "\"" << std::endl;
                        return 1;
                    }

                    if (it != std::string::npos)
                    {
                        fargs = fargs.substr(it+1);
                    }
                } while (it != std::string::npos);
#if DEBUG_PRINT
                std::cout << "debug: filters={";
                for (const auto &it : filters) std::cout << it.first << " ";
                std::cout << "}" << std::endl;
#endif
            }
            else
            {
                std::cerr << "error: missing argument for filters" << std::endl;
                return 1;
            }
        }
        else if (std::strncmp(argv[i], "-p", 3) == 0 || std::strncmp(argv[i], "--print", 8) == 0)
        {
            explicitPrint = true;
            i += 1;
            if (i < argc && argv[i][0] != '-')
            {
                printflags = OutputFlags::None;
                for (int j = 0; argv[i][j] != '\0'; ++j)
                {
                    switch (argv[i][j])
                    {
                        case 'n':
                            printflags |= OutputFlags::Name;
                            continue;
                        case 'h':
                            printflags |= OutputFlags::Hash;
                            continue;
                        case 'H':
                            printflags |= OutputFlags::NameHash;
                            continue;
                        case 'l':
                            printflags |= OutputFlags::LinkName;
                            continue;
                        case 'f':
                            printflags |= OutputFlags::Flags;
                            continue;
                        case 'c':
                            printflags |= OutputFlags::ChunksShort;
                            continue;
                        case 'C':
                            printflags |= OutputFlags::ChunksFull;
                            continue;
                        case 'd':
                            printflags |= OutputFlags::Default;
                            continue;
                        case 'v':
                            printflags |= OutputFlags::Verbose;
                            continue;
                        case 'a':
                            printflags = OutputFlags::All;
                            break;
                        default:
                            std::cerr << "warn: invalid printflag '" << argv[i][j] << "', valid flags are [nhHslfcCdva]" << std::endl;
                    }
                }
            }
        }
        else if (argv[i][0] == '-' && argv[i][1] == '-' && argv[i][2] == '\0')
        {
            i += 1;
            if (i < argc)
            {
                if (!filename.empty())
                {
                    std::cerr << "warn: \"" << argv[i] << "\" overwrites previous filename argument \"" << filename << "\"" << std::endl;
                }
                filename.assign(argv[i]);
            }
            break;
        }
        // treat any non-flag arguments as input
        else if (argv[i][0] != '-')
        {
            if (!filename.empty())
            {
                std::cerr << "warn: \"" << argv[i] << "\" overwrites previous filename argument \"" << filename << "\"" << std::endl;
            }
            filename.assign(argv[i]);
        }
        else
        {
            std::cerr << "error: invalid argument: " << argv[i] << std::endl;
            return 1;
        }
    }
    if (filename.empty())
    {
        std::cerr << "error: input file required" << std::endl;
        return 1;
    }

    const manifest::Reader reader(std::move(filename));
    if (reader.isError())
    {
        std::cerr << "error: failed to read manifest file" << std::endl;
        return 1;
    }
    std::cout << "info: manifest parsed successfully" << std::endl;
    if (datadir.empty() || explicitPrint)
    {
        reader.print(filters, printflags);
    }
    if (!datadir.empty())
    {
        reader.verify(filters, std::move(datadir), !verifyAll);
    }
    return reader.isError();
}
