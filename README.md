# steam-manifest-reader

## Description
Simple steam manifest file reader, with minimal dependencies (C++23 compiler required).

Parses the given steam depotcache manifest file and prints the contents (ie information about the listed files), or uses the contained sha1 checksums to verify files on disk.

Sample manifest files are provided in `samples` folder.

## Building (GCC)
`g++ -std=c++23 -o smr main.cpp`

or `g++ -g -Wall -DDEBUG_PRINT=1 -std=c++23 -o smr main.cpp`

or `g++ -O3 -flto -static -s -std=c++23 -o smr main.cpp`

## Usage
`./smr <manifest-file>`
